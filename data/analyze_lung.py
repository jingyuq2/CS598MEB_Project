#!/usr/local/bin/python
import os
import sys
import copy
import ast

label_trees = []
tree_map = {}
sig_map = {}

def genPaths(root):
	if tree_map[root] == []:
		return [[sig_map[root]]]
	res = []
	for child in tree_map[root]:
		for path in genPaths(child):
			res.append([sig_map[root]] + path)
	return res

for f in os.listdir("./lung/cluster"):
	label_trees.append(f)

for tree in label_trees:
	with open("./lung/cluster/" + tree) as f:
		tree_map = ast.literal_eval(f.readline())
		f.readline()
		sig_map = ast.literal_eval(f.readline())
		# print(tree_map)
		# print(sig_map)

	# simplify sig_map
	for node, exp in sig_map.items():
		new_exp = {}
		for sig in exp:
			new_exp[sig.split('.')[1]] = exp[sig]
		sig_map[node] = new_exp

	potential_roots = []
	root = ""
	childs = set()
	for node1 in tree_map:
		for node2 in tree_map[node1]:
			childs.add(node2)
		potential_roots.append(node1)
	for node in potential_roots:
		if node not in childs:
			root = node

	paths = genPaths(root)
	with open("./analysis/lung/" + tree, "w") as f:
		f.write(str(paths))
	