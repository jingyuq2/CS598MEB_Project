#!/usr/local/bin/python
import os
import sys
import copy

#cancer = sys.argv[1]
cancer = "OV"
pids = []

for f in os.listdir("./" + cancer):
	if f[-4:] != ".csv" and f != ".DS_Store" and "label" not in f:
		pids.append(f)

for pid in pids:
	parent_to_childs = {}
	nodes = set()  # store all node names
	with open("./" + cancer + "/" + pid) as f:
		f.readline()
		for l in f.readlines():
			l = l[:-1].replace('\"', '')
			parent = l.split(',')[1]
			child = l.split(',')[2]
			nodes.add(parent)
			nodes.add(child)
			if parent not in parent_to_childs:
				parent_to_childs[parent] = []
			parent_to_childs[parent].append(child)

		# handle leaf nodes
		for node in nodes:
			if node not in parent_to_childs:
				parent_to_childs[node] = []

	shift_it = 0
	while os.path.isfile("./" + cancer + "/" + pid + "_s" + str(shift_it) + ".csv"):
		# print(shift_it)  # test
		clusters = {}  # map nodes (except min node) to min node in the cluster
		cluster_list = []
		exposures = {}
		parent_to_childs_copy = copy.deepcopy(parent_to_childs)
		with open("./" + cancer + "/" + pid + "_s" + str(shift_it) + ".csv") as f:
			l = f.readline()[:-1].replace('\"', '')
			if l == '':
				break

			for c in l.split(',')[1:]:
				nodes = c.split(';')
				min_node = min(nodes)
				for node in nodes:
					if node != min_node:
						clusters[node] = min_node
				cluster_list.append(min_node)

			for l in f.readlines():
				cols = l[:-1].replace('\"', '').split(',')
				signature = cols[0]
				cols = cols[1:]
				for col in range(len(cols)):
					if cluster_list[col] not in exposures:
						exposures[cluster_list[col]] = {}
					exposures[cluster_list[col]][signature] = float(cols[col])

			# print(exposures)  # test

		# print(parent_to_childs_copy)  # test

		for parent in parent_to_childs_copy:
			childs = parent_to_childs_copy[parent]
			new_childs = []  # cluster child list
			for child in childs:
				if child in clusters:
					if clusters[child] != parent:
						new_childs.append(clusters[child])
				else:
					new_childs.append(child)
			new_childs = list(set(new_childs))
			parent_to_childs_copy[parent] = new_childs

			if parent in clusters:
				parent_to_childs_copy[clusters[parent]] += new_childs
				parent_to_childs_copy[clusters[parent]] = list(set(parent_to_childs_copy[clusters[parent]]))

		# delete clustered extra nodes
		for parent in clusters:
			del parent_to_childs_copy[parent]

		# handle case where clustered extra node has a child assigned to the same cluster
		for parent in parent_to_childs_copy:
			if parent in parent_to_childs_copy[parent]:
				parent_to_childs_copy[parent].remove(parent)

		with open('./' + cancer + '/' + pid + '_label_' + str(shift_it), 'w') as f:
			f.write(str(parent_to_childs_copy))
			f.write('\n---------\n')
			f.write(str(exposures))

		#print(parent_to_childs_copy)  # test
		#print(cluster_list)  # test
		#break  # test

		shift_it += 1

	# break
