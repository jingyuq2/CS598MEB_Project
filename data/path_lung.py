#!/usr/local/bin/python
import os
import sys
import ast
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import axes3d, Axes3D
from sklearn.datasets.samples_generator import make_blobs
from sklearn.cluster import KMeans

pids = set()
label_trees = []
sig = sys.argv[1]
label = '3'

if not os.path.isdir("./sigpath/" + sig):
	os.system("mkdir ./sigpath/" + sig)

for f in os.listdir("./lung/cluster"):
	pids.add(f[5:8])

# for f in os.listdir("./lung/cluster"):
# 	if f.split('_')[1] == '1' and f.split('_')[3] == label:
# 		label_trees.append(f)

for i in pids:
	name = "CRUK0" + i + "_1_label_"
	if os.path.isfile("./lung/cluster/" + name + "3"):
		label_trees.append(name + "3")
	elif os.path.isfile("./lung/cluster/" + name + "2"):
		label_trees.append(name + "2")
	elif os.path.isfile("./lung/cluster/" + name + "1"):
		label_trees.append(name + "1")

with open("./sigpath/" + sig + "/res", 'w') as f:
	for tree in label_trees:
		paths = None
		with open("./analysis/lung/" + tree) as f2:
			paths = ast.literal_eval(f2.readline())
		
		for path in paths:
			sigpath = []
			for exp in path:
				sigpath.append(exp[sig])
			f.write(str(sigpath) + '\n')
		f.write("------\n")

num_patient = 0
num_with_path_len = {}  # 'x': num of patient with at least one path of length >= x
paths = []
character_map = {'+-': 0, '+': 0, '-+': 0, '-': 0, '--': 0, '++': 0}  # key with two character; '-': decrease, '+': increase, '0': equal
path_3 = []
path_3_InDe = []
path_2 = []
path_2_De = []
path_2_In = []
with open("./sigpath/" + sig + "/res", "r") as f:
	for line in f.readlines():
		line = line[:-1]
		if line == "------":
			num_patient += 1
			for path in paths:
				if len(path) >= 3:
					if '3' not in num_with_path_len:
						num_with_path_len['3'] = 0
					num_with_path_len['3'] += 1
					break
			for path in paths:
				if len(path) >= 2:
					if '2' not in num_with_path_len:
						num_with_path_len['2'] = 0
					num_with_path_len['2'] += 1
					break

			char_list = []
			for path in paths:
				if len(path) >= 3:
					char = ""
					for i in range(2):
						if path[i + 1] > path[i]:
							char += '+'
						elif path[i + 1] < path[i]:
							char += '-'
						else:
							char += '0'
					char_list.append(char)
					if char == '+-':
						if path[1] >= 1.3 * path[0] and path[2] <= 0.7 * path[1]:
							path_3_InDe.append(path[0:3] / np.linalg.norm(path[0:3]))
					path_3.append(path[0:3])
				
				if len(path) >= 2:
					char = ""
					if path[1] > path[0]:
						char += '+'
					elif path[1] < path[0]:
						char += '-'
					else:
						char += '0'
					char_list.append(char)
					if char == '-':
						if path[0] * 0.7 >= path[1] and path[0] > 0.2:
							path_2_De.append(path[0:2] / np.linalg.norm(path[0:2]))
							break
					elif char == '+':
						if path[0] * 1.3 <= path[1] and path[1] > 0.2:
							path_2_In.append(path[0:2] / np.linalg.norm(path[0:2]))

					path_2.append(path[0:2])

			if '+-' in char_list:
				character_map['+-'] += 1
			if '+' in char_list:
				character_map['+'] += 1
			if '-+' in char_list:
				character_map['-+'] += 1
			if '-' in char_list:
				character_map['-'] += 1
			if '--' in char_list:
				character_map['--'] += 1
			if '++' in char_list:
				character_map['++'] += 1

			paths = []
		else:
			path = ast.literal_eval(line)
			paths.append(path)


print("#patients: " + str(num_patient))
print(num_with_path_len)
print(character_map)
print("-----")


# # k-means cluster on path_3
# wcss = []
# for i in range(1, 20):
# 	kmeans = KMeans(n_clusters=i, random_state=0)
# 	kmeans.fit(path_3)
# 	print("#cluster: " + str(i))
# 	print("Avg error: " + str(kmeans.inertia_))
# 	wcss.append(kmeans.inertia_)
# 	print(kmeans.cluster_centers_)
# 	print("*****")
# plt.plot(range(1, 20), wcss)
# plt.title('Elbow Method')
# plt.xlabel('Number of clusters')
# plt.ylabel('WCSS')
# plt.show()

# # k-means cluster on path_3 signature 1
# # Result: {"+-": 31, "++": 12, "-+": 15, "--": 6}, 25/31 have significant +- trend (>=30%)
# print("#path of length >= 3: " + str(len(path_3)))
# kmeans = KMeans(n_clusters=9, random_state=0)
# kmeans.fit(path_3)
# print(kmeans.cluster_centers_)
# label_dist = {}
# for lb in kmeans.labels_:
# 	if lb not in label_dist:
# 		label_dist[lb] = 0
# 	label_dist[lb] += 1
# print(label_dist)

# X = np.array(path_3)
# ax = plt.axes(projection = '3d')
# ax.scatter(X[:,0],X[:,1],X[:,2], c=kmeans.predict(X) , cmap='Set2', s=50)
# ax.set_xlabel('Level 0')
# ax.set_ylabel('Level 1')
# ax.set_zlabel('Level 2')
# plt.show()


# # k-means cluster on path_2
# wcss = []
# for i in range(1, 20):
# 	kmeans = KMeans(n_clusters=i, random_state=0)
# 	kmeans.fit(path_2)
# 	print("#cluster: " + str(i))
# 	print("Avg error: " + str(kmeans.inertia_))
# 	wcss.append(kmeans.inertia_)
# 	# print(kmeans.cluster_centers_)
# 	print("*****")
# plt.plot(range(1, 20), wcss)
# plt.title('Elbow Method')
# plt.xlabel('Number of clusters')
# plt.ylabel('WCSS')
# plt.show()

# # k-means cluster on path_2 signature 1
# # Result: 
# # Signature 1: {"+": 127, "-": 57}, 108/127 have significant + trend (>=30%)
# print("#path of length >= 2: " + str(len(path_2)))
# kmeans = KMeans(n_clusters=6, random_state=0)
# kmeans.fit(path_2)
# print(kmeans.cluster_centers_)
# label_dist = {}
# for lb in kmeans.labels_:
# 	if lb not in label_dist:
# 		label_dist[lb] = 0
# 	label_dist[lb] += 1
# print(label_dist)

# # k-means cluster on path_2 signature 4, 5
# # Result: 
# # Signature 4: {"+": 0, "-": 184}, 156/184 have significant + trend (>=30%)
# # Signature 5: {"+": 25, "-": 159}, 147/159 have significant + trend (>=30%)
# print("#path of length >= 2: " + str(len(path_2)))
# kmeans = KMeans(n_clusters=7, random_state=0)
# kmeans.fit(path_2)
# print(kmeans.cluster_centers_)
# label_dist = {}
# for lb in kmeans.labels_:
# 	if lb not in label_dist:
# 		label_dist[lb] = 0
# 	label_dist[lb] += 1
# print(label_dist)

# X = np.array(path_2)
# plt.scatter(X[:, 0], X[:, 1], c=kmeans.predict(X), s=50, cmap='viridis')
# plt.show()

# # k-means cluster on path_2 signature 6/13
# # Result: mostly 0
# print("#path of length >= 2: " + str(len(path_2)))
# kmeans = KMeans(n_clusters=7, random_state=0)
# kmeans.fit(path_2)
# print(kmeans.cluster_centers_)
# label_dist = {}
# for lb in kmeans.labels_:
# 	if lb not in label_dist:
# 		label_dist[lb] = 0
# 	label_dist[lb] += 1
# print(label_dist)

# # k-means cluster on path_2 signature 17
# # Result: mostly 0
# print("#path of length >= 2: " + str(len(path_2)))
# kmeans = KMeans(n_clusters=5, random_state=0)
# kmeans.fit(path_2)
# print(kmeans.cluster_centers_)
# label_dist = {}
# for lb in kmeans.labels_:
# 	if lb not in label_dist:
# 		label_dist[lb] = 0
# 	label_dist[lb] += 1
# print(label_dist)

# print(len(path_3_InDe))
# for i in range(len(path_3_InDe)):
# 	# print(path_3_InDe[i])
# 	plt.plot(['level 0', 'level 1', 'level 2'], path_3_InDe[i])
# 	plt.ylabel('Relative Exposure (Normalized)')

# for i in range(len(path_2_De)):
# 	# print(path_3_InDe[i])
# 	plt.plot(['level 0', 'level 1'], path_2_De[i])
# 	plt.ylabel('Relative Exposure (Normalized)')

# for i in range(len(path_2_In)):
# 	# print(path_3_InDe[i])
# 	plt.plot(['level 0', 'level 1'], path_2_In[i])
# 	plt.ylabel('Relative Exposure (Normalized)')

# plt.show()
