#!/usr/local/bin/python
import numpy as np
import matplotlib.pyplot as plt

labels = ['level 0', 'level 1', 'level 2']
sig1 = [[0.984, 0.773, 0.844], [0.876, 0.844, 1], [0.903, 0.774, 0.878], [0.602, 0.481, 0.568], [1, 0.895, 0.881], [1, 1, 1]]
sig3 = [[0.016, 0.227, 0.156], [0.124, 0.156, 0], [0.037, 0.187, 0.11], [0.371, 0.483, 0.392], [0, 0.105, 0.119], [0, 0, 0]]
patient = ['p1', 'p3', 'p4', 'p7', 'p10', 'p2 and p9']

fig, ax = plt.subplots()
width = 0.35

for i in range(len(sig1)):
	plt.subplot(2, 3, i + 1)
	plt.bar(labels, sig1[i], width, label='Signature 1')
	plt.bar(labels, sig3[i], width, bottom=sig1[i], label='Signature 3')
	plt.ylabel('Relative Exposure')
	plt.legend(loc = 'lower right')
	plt.title(patient[i])

plt.show()

# ax.bar(labels, sig1[0], width, label='Signature 1')
# ax.bar(labels, sig3[0], width, bottom=sig1[0], label='Signature 3')

# ax.set_ylabel('Relative Exposure')
# # ax.set_title('Scores by group and gender')
# ax.legend()

plt.show()
